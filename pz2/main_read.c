#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define DEV_NAME "foo"
#define SIZE_BUF 20
#define READ_SIZE 20

int main() {
    char path[100];
    int f;
    char buffer[SIZE_BUF];



    sprintf(path, "/dev/%s", DEV_NAME);

    f = open(path, O_RDWR);
    if (f == -1) {
        perror("Не удалось открыть файл \nОшибка");
        return 1;
    }
    else printf("Файл открыт \n");

    int read_bytes = read(f, buffer, 10);

    if (read_bytes == -1) {
        perror("Ошибка чтения из файла");
        return 1;
    }

    if (read_bytes > 0) {
        printf("Прочитано из файла: %s\n", buffer);
    } else {
        printf("Файл пуст\n");
    }



    close(f);
    return 0;
}
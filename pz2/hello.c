#include <linux/module.h>
#include <linux/fs.h>
#include <linux/printk.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>

#define BUF_LEN 100

static int Major;
static int already_open = 0;
static char buffer_dev[BUF_LEN];
static wait_queue_head_t wq;


static int device_open(struct inode *inode, struct file *file) {
    printk(KERN_INFO "File open!\n");
    already_open++;
    // sprintf(buffer_dev, "Hello world24!\n");
    return 0;
}

static int device_release(struct inode *inode, struct file *file) {
    already_open--;
    printk(KERN_INFO "File close!\n");
    printk(KERN_INFO "Number of openings: %d\n", already_open);
    return 0;
}

static ssize_t device_read(struct file *file, char __user *buffer, size_t length, loff_t *offset) {
    
    if((file->f_flags & O_NONBLOCK) == 0){
    wait_event_interruptible(wq, buffer_dev[0] != '\0');
    }
    // int bytes_read = 0;
    // int si = strlen(buffer_dev);

    raw_copy_to_user(buffer, buffer_dev, length);
    
    return 0;
}

static ssize_t device_write (struct file *file, const char __user *buffer, size_t length, loff_t *offset){
    int length_buffer = BUF_LEN - *offset;
    if (length_buffer - length <= 0){
        printk(KERN_INFO "No space on device");
        return -ENOSPC; //нет места на устройстве
    }

    if(raw_copy_from_user(&buffer_dev[*offset], buffer, length) != 0){
        return -EFAULT; // не правильный адрес
    }
    printk(KERN_INFO "Recording ended \n");
    printk(KERN_INFO "%s", buffer_dev);
    wake_up(&wq);
    return length;
}


const struct file_operations fops = {
    .open = device_open,
    .read = device_read,
    .release = device_release,
    .write = device_write
};


int init_module(void) {
    init_waitqueue_head(&wq);
    Major = register_chrdev(0, "foo", &fops);
    if (Major < 0) {
        printk(KERN_ALERT "Registering char device failed with %d\n", Major);
        return Major;
    }
    printk(KERN_INFO "I was assigned major number %d. To talk to the driver, create a dev file with\n", Major);
    printk(KERN_INFO "'mknod /dev/%s c %d 0'.\n", "foo", Major);
    return 0;
}

void cleanup_module(void) {
    pr_info("Goodbye world 1.\n");
    unregister_chrdev(Major, "foo"); 
   }

MODULE_LICENSE("GPL");

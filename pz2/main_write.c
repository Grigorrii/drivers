#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define DEV_NAME "foo"
#define SIZE_BUF 20
#define READ_SIZE 20

int main() {
    char path[100];
    int f;
    char buffer[SIZE_BUF];



    sprintf(path, "/dev/%s", DEV_NAME);
    sprintf(buffer, "Hello3");
    f = open(path, O_RDWR);
    
    int write_bytes = (int)write(f, buffer, strlen(buffer));
    if (write_bytes < 0) {
        perror("Ошибка записи");
        return 1;
    }

    close(f);
    return 0;
}
